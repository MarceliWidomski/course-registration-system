/*
 * Classroom.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "Classroom.h"
#include <iostream>
#include <string>
#include <sstream>

Classroom::Classroom() {
	number = 0;
	building = "";
	capacity = 0;
}
Classroom::Classroom(unsigned short number, std::string building,
		unsigned short capacity) {
	this->number = number;
	this->building = building;
	this->capacity = capacity;
}
void Classroom::showDescription() {
	this->getDescription();
	this->showCourses();
}
void Classroom::addCourse(courseSubject subject) {
	short i(0);
	for (i = 0; i < size; ++i) {
		if (coursesInRoom[i] == 0)
			break;
	}
	coursesInRoom[i] = subject;
}
void Classroom::showCourses() {
	std::cout << "Courses in room: ";
	int i(0);
	if (coursesInRoom[i] == 0)
		std::cout << "There is no course which take place in this room. "
				<< std::endl;
	else {
		while (coursesInRoom[i] != 0) {
			printSubject(coursesInRoom[i]);
			++i;
			if (coursesInRoom[i] != 0)
				std::cout << ", ";
		}
	}
	std::cout << std::endl;
}
bool Classroom::operator== (const Classroom& other){
if(	this->number == other.number && this->building == other.building && this->capacity == other.capacity)
	return 1;
else
	return 0;
}
std::string Classroom::getDescription(){
	std::string description("Classroom: ");
	std::stringstream ss;
	ss << this->getCapacity();
	description += this->getNumber() + ", building: " + this->getBuilding()
				+ "\nCapacity: " + ss.str() + " people";
	return description;
}
