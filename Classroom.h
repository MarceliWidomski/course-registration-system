/*
 * Classroom.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef CLASSROOM_H_
#define CLASSROOM_H_

#include "Describable.h"
#include "courseSubject.h"
#include <string>
const short size(100);

class Classroom: public Describable {
public:
	Classroom();
	Classroom(unsigned short number, std::string building, unsigned short capacity);
	void showDescription();
	void addCourse(courseSubject subject);
	void showCourses();

	const std::string& getBuilding() const {return building;}
	short getCapacity() const {return capacity;}
	short getNumber() const {return number;}

	bool operator== (const Classroom& other);
private:
	short number;
	std::string building;
	short capacity;
	courseSubject coursesInRoom[size]={};
	std::string getDescription();
};

#endif /* CLASSROOM_H_ */
