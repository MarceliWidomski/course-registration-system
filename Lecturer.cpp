/*
 * Lecturer.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "Lecturer.h"
#include <iostream>

Lecturer::Lecturer (){}
Lecturer::Lecturer(std::string firstName, std::string lastName, short age, std::string eMail) :
	Person (firstName, lastName, age, eMail){}
	Lecturer::~Lecturer(){}

void Lecturer::personality(){
	std::cout << "Lecturer " << getFirstName() << " " << getLastName() << " teaches at: ";
}
