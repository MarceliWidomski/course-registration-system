/*
 * Course.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef COURSE_H_
#define COURSE_H_

#include <string>
#include "Describable.h"
#include "Classroom.h"
#include "Person.h"
#include "Student.h"
#include "Lecturer.h"
#include "courseSubject.h"

const short arraySize (100);

class Course: public Describable {
public:
	Course ();
	Course(courseSubject subject, std::string startingDate, std::string finishingDate, short maxStudents=0, Classroom* classroom = 0);
	void showLecturers();
	void showStudents();
	void addLecturer (Lecturer *lecturer);
	void addStudent (Student *student);
	void addClassroom (Classroom *classroom);
	void showDescription ();

	bool operator==(const Course& other);

	const std::string& getFinishingDate() const {return finishingDate;}
	void setFinishingDate(const std::string& finishingDate) {this->finishingDate = finishingDate;}
	const unsigned short getPlacesAvaible() const {return placesAvaible;}
	void setPlacesAvaible(unsigned short placesAvaible) {this->placesAvaible = placesAvaible;}
	const std::string& getStartingDate() const {return startingDate;}
	void setStartingDate(const std::string& startingDate) {this->startingDate = startingDate;}
	courseSubject getSubject() const {return subject;}
	void setSubject(courseSubject subject) {this->subject = subject;}
	unsigned short getMaxStudents() const {return maxStudents;}
	void setMaxStudents(unsigned short maxStudents) {this->maxStudents = maxStudents;}

private:
	Classroom *classroom;
	std::string startingDate;
	std::string finishingDate;
	Lecturer* lecturer[arraySize] = { };
	Student* student[arraySize] = { };
	courseSubject subject;
	short maxStudents;
	short placesAvaible;
	std::string getDescription();
};

#endif /* COURSE_H_ */
