/*
 * Course.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "Course.h"
#include <iostream>
Course::Course() {
	subject = Biology;
	startingDate = "";
	finishingDate = "";
	maxStudents = 0;
	placesAvaible = 0;
	classroom = 0;
}
Course::Course(courseSubject subject, std::string startingDate,
		std::string finishingDate, short maxStudents, Classroom* classroom) {
	this->subject = subject;
	this->startingDate = startingDate;
	this->finishingDate = finishingDate;
	this->maxStudents = maxStudents;
	this->placesAvaible = maxStudents;
	this->classroom = classroom;
}
void Course::addLecturer(Lecturer *lecturer) {
	for (int i = 0; i < arraySize; ++i) {
		if (this->lecturer[i] == 0) {
			this->lecturer[i] = lecturer;
			lecturer->addCourse(this);
			break;
		}
	}
}
void Course::showLecturers() {
	std::cout << "Lecturers who teach at this course: " << std::endl;
	int i(0);
	while (lecturer[i] != 0) {
		std::cout << i + 1 << ". " << lecturer[i]->getFirstName() << " "
				<< lecturer[i]->getLastName() << std::endl;
		++i;
	}
}
void Course::addStudent(Student *student) {
	if (placesAvaible > 0) {
		for (int i = 0; i < maxStudents; ++i) {
			if (this->student[i] == 0) {
				this->student[i] = student;
				student->addCourse(this);
				--placesAvaible;
				std::cout
						<< "Congratulations! You've been successfully enrolled in course!"
						<< std::endl;
				break;
			} else if (this->student[i]->getFirstName()
					== student->getFirstName()
					&& this->student[i]->getLastName()
							== student->getLastName()) {
				this->student[i] = student;
				std::cout << "Student is already enrolled in this course."
						<< std::endl;
				break;
			}
		}
	} else
		std::cout << "No places available." << std::endl;
}
void Course::showStudents() {
	std::cout << "Students who take part in this course: " << std::endl;
	int i(0);
	while (student[i] != 0) {
		std::cout << i + 1 << ". " << student[i]->getFirstName() << " "
				<< student[i]->getLastName() << std::endl;
		++i;
	}
}
void Course::showDescription() {
	std::cout << this->getDescription() << std::endl;
	std::cout << "Classroom: " << classroom->getNumber() << ", building: "
			<< classroom->getBuilding() << std::endl;
	std::cout << "Max number of students: " << this->getMaxStudents()
			<< std::endl;
	std::cout << "Available places: " << this->getPlacesAvaible() << std::endl;
	this->showLecturers();
	this->showStudents();
}
void Course::addClassroom(Classroom *classroom) {
	this->classroom = classroom;
	this->classroom->addCourse(this->subject);
}
std::string convertSubjectToString(courseSubject subject) {
	std::string codeArray[7] = { "Biology", "Chemistry", "Physics", "Maths",
			"English", "Spanish", "French" };
	return codeArray[subject - 1];
}
std::string Course::getDescription() {
	std::string description("Course details: "); // this notation makes string of "Course..." instead array of chars which allows to add objects to it
	description = description + "\nSubject: " + convertSubjectToString(this->getSubject())
			+ "\nStarting date: " + this->getStartingDate()
			+ "\nFinishing date: " + this->getFinishingDate();
	return description;
}
bool Course::operator==(const Course& other) {
	if (this->subject == other.subject
			&& this->startingDate == other.startingDate
			&& this->finishingDate == other.finishingDate
			&& this->maxStudents == other.maxStudents
			&& this->classroom == other.classroom
			&& this->placesAvaible == other.placesAvaible)
		return 1;
	else
		return 0;
}

