/*
 * Person.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef PERSON_H_
#define PERSON_H_

#include <string>
class Course;

const short maxSize (100);
class Person {
public:
	Person();
	Person(std::string firstName, std::string lastName, short age=0, std::string eMail="");
	virtual ~Person();
	void showCourses();
	void addCourse(Course* course);
	void enterPersonDetails();
	std::string getDescription();

	short getAge() const {return age;}
	void setAge(short age) {this->age = age;}
	const std::string& getFirstName() const {return firstName;}
	void setFirstName(const std::string& firstName) {this->firstName = firstName;}
	const std::string& getLastName() const {return lastName;}
	void setLastName(const std::string& lastName) {this->lastName = lastName;}
	const std::string& getMail() const {return eMail;}
	void setMail(const std::string& mail) {eMail = mail;}

	bool operator==(const Person& other);

private:
	std::string firstName;
	std::string lastName;
	Course* aCourse[maxSize]={};
	std::string eMail;
	short age;
	virtual void personality() = 0;


};

#endif /* PERSON_H_ */
