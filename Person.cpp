/*
 * Person.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "Person.h"
#include "Course.h"
#include <iostream>

Person::Person() {
	firstName = "";
	lastName = "";
	age = 0;
	eMail = "";
}
Person::Person(std::string firstName, std::string lastName, short age,
		std::string eMail) {
	this->firstName = firstName;
	this->lastName = lastName;
	this->age = age;
	this->eMail = eMail;
}
Person::~Person(){}
void Person::showCourses() {
	personality();
	int i(0);
	if (aCourse[i] == 0)
		std::cout << "This person doesn't take part in any course. "
				<< std::endl;
	while (aCourse[i] != 0) {
		printSubject(aCourse[i]->getSubject());
		++i;
		if (aCourse[i] != 0)
			std::cout << ", ";
	}
	std::cout << std::endl;
}
void Person::enterPersonDetails() {
	std::cout << "First name: ";
	std::string name;
	std::cin >> name;
	this->setFirstName(name);
	std::cout << "Last name: ";
	std::cin >> name;
	this->setLastName(name);
	std::cout << "Age: ";
	short age;
	std::cin >> age;
	this->setAge(age);
	std::cout << "Mail adress: ";
	std::string mail;
	std::cin >> mail;
	this->setMail(mail);
}
void Person::addCourse(Course* course) {
	short i(0);
	while (aCourse[i] != 0) {
		++i;
	}
	aCourse[i] = course;
}
bool Person::operator==(const Person& other) {
	if (this->firstName == other.firstName && this->lastName == other.lastName
			&& this->eMail == other.eMail && this->age == other.age)
		return 1;
	else
		return 0;
}
std::string Person::getDescription() {
	std::string description("First name: ");
	description += this->getFirstName()
				+ "\nLast name: " + this->getLastName()
				+ "\nMail: " + this->getMail();
	return description;
}
