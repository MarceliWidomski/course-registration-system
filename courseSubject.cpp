/*
 * courseSubject.cpp
 *
 *  Created on: 02.04.2017
 *      Author: marce
 */
#include "courseSubject.h"
#include <iostream>

void printSubject( courseSubject subject){
	if (subject == Biology)
		std::cout << "Biology";
	else if (subject == Chemistry)
			std::cout << "Chemistry";
	else if (subject == Physics)
				std::cout << "Physics";
	else if (subject == Maths)
				std::cout << "Maths";
	else if (subject == English)
				std::cout << "English";
	else if (subject == Spanish)
				std::cout << "Spanish";
	else if (subject == French)
				std::cout << "French";
	else
		std::cout << "Invalid course subject" << std::endl;
}


