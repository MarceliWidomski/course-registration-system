//============================================================================
// Name        : courses.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Course.h"
#include "Classroom.h"
#include "Person.h"
#include "Student.h"
#include "Lecturer.h"
#include "Database.h"
#include "RegistrationSystem.h"

int main() {

	Database database("Courses database");

	database.createLecturer("Roger", "Federer", 35, "roger.federer@gmail.com");
	database.createLecturer("Andy", "Murray", 29, "andy.murray@gmail.com");
	database.createLecturer("Rafael", "Nadal", 30, "rafael.nadal@gmail.com");
	database.createLecturer("Novak", "Djokovic", 29, "novak.djokovic@gmail.com");
	database.createLecturer("Stan", "Wawrinka", 32, "stan.wawrinka@gmail.com");

	database.createClassroom(109, "Department of Mathematics", 30);
	database.createClassroom(222, "Language Center", 18);
	database.createClassroom(314, "Department of Life Sciences", 20);

	database.createCourse(Biology, "7 June 2017", "14 September 2017", 20);
	database.createCourse(Chemistry, "18 April 2017", "24 June 2017", 12);
	database.createCourse(Physics, "22 April 2017", "1 July 2017", 15);
	database.createCourse(Maths, "3 May 2017", "10 July 2017", 25);
	database.createCourse(English, "3 May 2017", "15 July 2017", 15);
	database.createCourse(Spanish, "22 May 2017", "10 September 2017", 15);
	database.createCourse(French, "15 May 2017", "7 September 2017", 15);

	database.addLecturerToCourse(Biology, "Roger", "Federer");
	database.addLecturerToCourse(Biology, "Andy", "Murray");
	database.addLecturerToCourse(Chemistry, "Roger", "Federer");
	database.addLecturerToCourse(Chemistry, "Rafael", "Nadal");
	database.addLecturerToCourse(Physics, "Roger", "Federer");
	database.addLecturerToCourse(Physics, "Novak", "Djokovic");
	database.addLecturerToCourse(Maths, "Roger", "Federer");
	database.addLecturerToCourse(Maths, "Rafael", "Nadal");
	database.addLecturerToCourse(English, "Stan", "Wawrinka");
	database.addLecturerToCourse(Spanish, "Stan", "Wawrinka");
	database.addLecturerToCourse(French, "Stan", "Wawrinka");

	database.addClassroomToCourse(Biology, 314, "Department of Life Sciences");
	database.addClassroomToCourse(Chemistry, 314, "Department of Life Sciences");
	database.addClassroomToCourse(Physics, 314, "Department of Life Sciences");
	database.addClassroomToCourse(Maths, 109, "Department of Mathematics");
	database.addClassroomToCourse(English, 222, "Language Center");
	database.addClassroomToCourse(Spanish, 222, "Language Center");
	database.addClassroomToCourse(French, 222, "Language Center");

	RegistrationSystem registrationSystem(&database);
	registrationSystem.run();

	return 0;
}
