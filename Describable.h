/*
 * Describable.h
 *
 *  Created on: 07.04.2017
 *      Author: RENT
 */

#ifndef DESCRIBABLE_H_
#define DESCRIBABLE_H_
#include <string>

class Describable {
public:
	Describable();
	virtual std::string getDescription()=0;
	virtual ~Describable(){};
};

#endif /* DESCRIBABLE_H_ */
