/*
 * RegistrationSystem.cpp
 *
 *  Created on: 15.04.2017
 *      Author: marce
 */

#include "RegistrationSystem.h"
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

RegistrationSystem::RegistrationSystem(Database* database) :
		database(database) {}

RegistrationSystem::~RegistrationSystem() {}

void RegistrationSystem::showRegistrationMenu() {
	cout << "Welcome to our academy!" << endl;
	cout << "How can we help you?" << endl;
	cout << "1) Enroll in  course" << endl;
	cout << "2) Show course description" << endl;
	cout << "3) Show lecturer's courses" << endl;
	cout << "4) Show student's courses" << endl;
	cout << "5) Show courses in classroom" << endl;
	cout << "6) Quit" << endl;
}
short RegistrationSystem::enterOption() {
	short option;
	cout << "Choose option: ";
	cin >> option;
	return option;
}
void RegistrationSystem::showCourseMenu() {
	cout << "We offer following courses:" << endl;
	cout << "1) Biology" << endl;
	cout << "2) Chemistry" << endl;
	cout << "3) Physics" << endl;
	cout << "4) Maths" << endl;
	cout << "5) English" << endl;
	cout << "6) Spanish" << endl;
	cout << "7) French" << endl;
}
courseSubject RegistrationSystem::setSubject(short option) {
	courseSubject subject;
	switch (option) {
	case 1:
		subject = Biology;
		break;
	case 2:
		subject = Chemistry;
		break;
	case 3:
		subject = Physics;
		break;
	case 4:
		subject = Maths;
		break;
	case 5:
		subject = English;
		break;
	case 6:
		subject = Spanish;
		break;
	case 7:
		subject = French;
		break;
	}
	return subject;
}
courseSubject RegistrationSystem::chooseSubject() {
	short option2(0);
	courseSubject subject;
	while (option2 == 0) {
		showCourseMenu();
		option2 = enterOption();
		if (option2 > 0 && option2 < 8) {
			subject = setSubject(option2);
		} else {
			cout << "Invalid option chosen. Try again." << endl;
			option2 = 0;
			enterToContinue();
		}
	}
	return subject;
}
std::string RegistrationSystem::enterFirstName() {
	cout << "Enter person's first name: ";
	std::string firstName;
	cin >> firstName;
	return firstName;
}
std::string RegistrationSystem::enterLastName() {
	cout << "Enter person's last name: ";
	std::string lastName;
	cin >> lastName;
	return lastName;
}
short RegistrationSystem::enterRoomNumber() {
	cout << "Enter classroom number: ";
	short roomNumber;
	cin >> roomNumber;
	return roomNumber;
}
void RegistrationSystem::enterToContinue() {
	cout << "Press ENTER to continue" << endl;
	cin.get();
	cin.get();
}
std::string RegistrationSystem::enterBuilding() {
	cout << "Enter building name: ";
	std::string building;
	cin >> building;
	return building;
}
void RegistrationSystem::run() {
	short option(0);
	courseSubject subject;
	while (option != 6) {
		showRegistrationMenu();
		option = enterOption();
		switch (option) {
		case 1: {
			subject = chooseSubject();
			database->setPointerAtStudent("", "");
			database->pStudent->enterPersonDetails();
			database->addStudentToCourse(subject,
					database->pStudent->getFirstName(),
					database->pStudent->getLastName());
			enterToContinue();
			break;
		}
		case 2: {
			subject = chooseSubject();
			database->setPointerAtCourse(subject);
			if (database->pCourse != 0)
				database->pCourse->showDescription();
			enterToContinue();
			break;
		}
		case 3: {
			std::string firstName = enterFirstName();
			std::string lastName = enterLastName();
			database->setPointerAtLecturer(firstName, lastName);
			if (database->pLecturer != 0)
				database->pLecturer->showCourses();
			enterToContinue();
			break;
		}
		case 4: {
			std::string firstName = enterFirstName();
			std::string lastName = enterLastName();
			database->setPointerAtStudent(firstName, lastName);
			if (database->pStudent != 0)
				database->pStudent->showCourses();
			enterToContinue();
			break;
		}
		case 5: {
			database->printAvailableRooms();
			short roomNumber = enterRoomNumber();
			std::string building = enterBuilding();
			database->setPointerAtClassroom(roomNumber, building);
			if (database->pClassroom != 0)
				database->pClassroom->showCourses();
			enterToContinue();
			break;
		}
		case 6: {
			cout << "Goodbye, we hope to see you soon!" << endl;
			break;
		}
		default: {
			cout << "Invalid option chosen." << endl;
			enterToContinue();
			break;
		}
		}
	}
}

