/*
 * Database.h
 *
 *  Created on: 09.04.2017
 *      Author: marce
 */

#ifndef DATABASE_H_
#define DATABASE_H_

#include "Course.h"
#include "Classroom.h"
#include "Person.h"
#include "Student.h"
#include "Lecturer.h"
#include <string>

const short arraySize2(50);

class Database {
public:
	Database();
	Database(std::string name);
	~Database();
	void addStudent(Student* student);
	void createStudent(std::string firstName, std::string lastName, short age = 0,
			std::string eMail = "");
	void addLecturer(Lecturer* lecturer);
	void createLecturer(std::string firstName, std::string lastName, short age = 0,
			std::string eMail = "");
	void addClassroom(Classroom* clasrrom);
	void createClassroom(unsigned short number, std::string building,
			unsigned short capacity);
	void addCourse(Course* course);
	void createCourse(courseSubject subject, std::string startingDate,
			std::string finishingDate, short maxStudents = 0,
			Classroom* classroom = 0);
	void setPointerAtStudent(std::string firstName, std::string lastName);
	void setPointerAtLecturer(std::string firstName, std::string lastName);
	void setPointerAtClassroom(short number, std::string buildind);
	void setPointerAtCourse(courseSubject subject);
	void addLecturerToCourse (courseSubject courseSubject, std::string lecturerFirstName, std::string lecturerLastName);
	void addStudentToCourse (courseSubject courseSubject, std::string studentFirstName, std::string studentLastName);
	void addClassroomToCourse(courseSubject courseSubject, short classroomNumber, std::string building);
	void printAvailableRooms();
	void printAvailableCourses ();

	Course aCourse[arraySize2];
	Classroom aClassroom[arraySize2];
	Student aStudent[arraySize2];
	Lecturer aLecturer[arraySize2];

	Student* pStudent;
	Lecturer* pLecturer;
	Classroom* pClassroom;
	Course* pCourse;
private:
	std::string name;

};

#endif /* DATABASE_H_ */
