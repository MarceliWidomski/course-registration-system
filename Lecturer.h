/*
 * Lecturer.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef LECTURER_H_
#define LECTURER_H_

#include "Person.h"

class Lecturer: public Person {
public:
	Lecturer();
	Lecturer(std::string firstName, std::string lastName, short age=0, std::string eMail="");
	virtual ~Lecturer();
private:
	void personality();
};

#endif /* LECTURER_H_ */
