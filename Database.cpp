/*
 * Database.cpp
 *
 *  Created on: 09.04.2017
 *      Author: marce
 */

#include "Database.h"
#include <iostream>
using std::cout;
using std::endl;

Database::Database() {
	pStudent = 0;
	pLecturer = 0;
	pClassroom = 0;
	pCourse = 0;
}
Database::Database(std::string name) {
	this->name = name;
	pStudent = 0;
	pLecturer = 0;
	pClassroom = 0;
	pCourse = 0;
}
Database::~Database() {
}

void Database::addStudent(Student* student) {
	for (int i = 0; i < arraySize2; ++i) {
		if (aStudent[i].getFirstName() == "") {
			aStudent[i] = *student;
			cout << "Student added to database!" << endl;
			break;
		} else if (aStudent[i] == *student) {
			cout << "This student already exists in database!" << endl;
			break;
		} else if (i + 1 == arraySize2)
			cout << "Can't add this student. Student's database is full."
					<< endl;
	}
}

void Database::createStudent(std::string firstName, std::string lastName,
		short age, std::string eMail) {
	Student temporaryStudent(firstName, lastName, age, eMail);
	addStudent(&temporaryStudent);
}
void Database::addLecturer(Lecturer* lecturer) {
	for (int i = 0; i < arraySize2; ++i) {
		if (aLecturer[i].getFirstName() == "") {
			aLecturer[i] = *lecturer;
//			cout << "Lecturer added to database!" << endl;
			break;
		} else if (aLecturer[i] == *lecturer) {
			cout << "This lecturer already exists in database!" << endl;
			break;
		} else if (i + 1 == arraySize2)
			cout << "Can't add this lecturer. Lecturer's database is full."
					<< endl;
	}
}
void Database::createLecturer(std::string firstName, std::string lastName,
		short age, std::string eMail) {
	Lecturer temporaryLecturer(firstName, lastName, age, eMail);
	addLecturer(&temporaryLecturer);
}
void Database::addClassroom(Classroom* classroom) {
	for (int i = 0; i < arraySize2; ++i) {
		if (aClassroom[i].getNumber() == 0) {
			aClassroom[i] = *classroom;
//			cout << "Classroom added to database!" << endl;
			break;
		} else if (aClassroom[i] == *classroom) {
			cout << "This classroom already exists in database!" << endl;
			break;
		} else if (i + 1 == arraySize2)
			cout << "Can't add this classroom. Classroom's database is full."
					<< endl;
	}
}
void Database::createClassroom(unsigned short number, std::string building,
		unsigned short capacity) {
	Classroom temporaryClassroom(number, building, capacity);
	addClassroom(&temporaryClassroom);
}
void Database::addCourse(Course* course) {
	for (int i = 0; i < arraySize2; ++i) {
		if (aCourse[i].getStartingDate() == "") {
			aCourse[i] = *course;
//			cout << "Course added to database!" << endl;6
			break;
		} else if (aCourse[i] == *course) {
			cout << "This course already exists in database!" << endl;
			break;
		} else if (i + 1 == arraySize2)
			cout << "Can't add this course. Course's database is full." << endl;
	}
}
void Database::createCourse(courseSubject subject, std::string startingDate,
		std::string finishingDate, short maxStudents, Classroom* classroom) {
	Course temporaryCourse(subject, startingDate, finishingDate, maxStudents,
			classroom);
	addCourse(&temporaryCourse);
}
void Database::setPointerAtStudent(std::string firstName,
		std::string lastName) {
	for (int i = 0; i < arraySize2; ++i) {
		if (aStudent[i].getFirstName() == firstName
				&& aStudent[i].getLastName() == lastName) {
			pStudent = &aStudent[i];
			break;
		} else if (i + 1 == arraySize2) {
			cout << "Student doesn't exist in database" << endl;
			pStudent = 0;
		}
	}
}
void Database::setPointerAtLecturer(std::string firstName,
		std::string lastName) {
	for (int i = 0; i < arraySize2; ++i) {
		if (aLecturer[i].getFirstName() == firstName
				&& aLecturer[i].getLastName() == lastName) {
			pLecturer = &aLecturer[i];
			break;
		} else if (i + 1 == arraySize2) {
			cout << "Lecturer doesn't exist in database" << endl;
			pLecturer = 0;
		}
	}
}
void Database::setPointerAtClassroom(short number, std::string buildind) {
	for (int i = 0; i < arraySize2; ++i) {
		if (aClassroom[i].getNumber() == number
				&& aClassroom[i].getBuilding() == buildind) {
			pClassroom = &aClassroom[i];
			break;
		} else if (i + 1 == arraySize2) {
			cout << "Classroom doesn't exist in database" << endl;
			pClassroom = 0;
		}
	}
}
void Database::setPointerAtCourse(courseSubject subject) {
	for (int i = 0; i < arraySize2; ++i) {
		if (aCourse[i].getSubject() == subject) {
			pCourse = &aCourse[i];
			break;
		} else if (i + 1 == arraySize2) {
			cout << "Course doesn't exist in database" << endl;
			pCourse = 0;
		}
	}
}
void Database::addLecturerToCourse(courseSubject courseSubject,
		std::string lecturerFirstName, std::string lecturerLastName) {
	setPointerAtCourse(courseSubject);
	setPointerAtLecturer(lecturerFirstName, lecturerLastName);
	if (pCourse != 0 && pLecturer != 0)
		pCourse->addLecturer(pLecturer);
}
void Database::addStudentToCourse(courseSubject courseSubject,
		std::string studentFirstName, std::string studentLastName) {
	setPointerAtCourse(courseSubject);
	setPointerAtStudent(studentFirstName, studentLastName);
	if (pCourse != 0 && pStudent != 0)
		pCourse->addStudent(pStudent);
}
void Database::addClassroomToCourse(courseSubject courseSubject,
		short classroomNumber, std::string building) {
	setPointerAtCourse(courseSubject);
	setPointerAtClassroom(classroomNumber, building);
	if (pCourse != 0 && pClassroom != 0)
		pCourse->addClassroom(pClassroom);
}
void Database::printAvailableRooms() {
	cout << "Available rooms: ";
	for (int i = 0; i < arraySize; ++i) {
		cout << aClassroom[i].getNumber() << " " << aClassroom[i].getBuilding();
		if (aClassroom[i + 1].getNumber() == 0) {
			cout << endl;
			break;
		}
		cout << ", ";
	}
}
void Database::printAvailableCourses (){
	cout << "Available courses: ";
		for (int i = 0; i < arraySize; ++i) {
			cout << aCourse[i].getSubject();
			if (aCourse[i+1].getStartingDate() == "") {
				cout << endl;
				break;
			}
			cout << ", ";
		}
}
