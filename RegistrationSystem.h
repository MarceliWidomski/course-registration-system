/*
 * RegistrationSystem.h
 *
 *  Created on: 15.04.2017
 *      Author: marce
 */

#ifndef REGISTRATIONSYSTEM_H_
#define REGISTRATIONSYSTEM_H_


#include "Database.h"
#include <string>

class RegistrationSystem {
public:
	RegistrationSystem(Database* database);
	~RegistrationSystem();
	void run();
private:
	Database* const database;
	void showRegistrationMenu();
	short enterOption();
	void showCourseMenu();
	courseSubject chooseSubject();
	courseSubject setSubject(short option);
	std::string enterFirstName();
	std::string enterLastName();
	std::string enterBuilding();
	short enterRoomNumber();
	void enterToContinue();
};

#endif /* REGISTRATIONSYSTEM_H_ */
